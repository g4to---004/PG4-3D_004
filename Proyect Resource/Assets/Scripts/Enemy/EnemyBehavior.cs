﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour
{
    // PATRULLAR
    public NavMeshAgent agent;

    public float speed;

    public float waitTimePatrol;
    public float startWaitTimePatrol;

    public List<Transform> moveSpots;
    public int indexSpot;
    Rigidbody enemyRigidbody;

    // LOOK (FIELD OF VIEW
    public float radius;
    public float angleView;
    
    // CHASE PLAYER

    bool playerFound = false;


    public bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        waitTimePatrol = startWaitTimePatrol;
        indexSpot = 0;
        enemyRigidbody = GetComponent<Rigidbody>();


    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();

        if (playerFound == false)
        {
            Patrol();
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees)
    {
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }


    void FixedUpdate()
    {
        
    }

    void AwakenEnemy()
    {

    }

    void Patrol()
    {
        Vector3 position = moveSpots[indexSpot].transform.position;
        agent.SetDestination(position);
        Debug.Log(Vector3.Distance(transform.position, position));
        if (Vector3.Distance(transform.position, position) < 0.5f)
        {
            if (waitTimePatrol <= 0)
            {
                indexSpot++;
                if (indexSpot > moveSpots.Count - 1)
                {
                    indexSpot = 0;
                }
                waitTimePatrol = startWaitTimePatrol;
            }
            else
            {
                waitTimePatrol -= Time.deltaTime;
            }


           
        }
    }

    void FindPlayer()
    {

    }
}
