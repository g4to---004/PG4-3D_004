﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolAI : MonoBehaviour
{
    // PATRULLAR
    float waitTimePatrol;
    public float startWaitTimePatrol;

    public List<Transform> moveSpots;
    public int indexSpot;
    

    // Start is called before the first frame update
    void Start()
    {
        waitTimePatrol = startWaitTimePatrol;
        indexSpot = 0;
    }
    
    

    public void PatrolBehavior(NavMeshAgent agent, FieldOfView fov)
    {
        FindPlayer(agent,fov);
        if (agent.isStopped)
        {
            ChaseTarget(agent,fov);
        }
    }

    void ChaseTarget(NavMeshAgent agent, FieldOfView fov)
    {
        agent.SetDestination(fov.visibleTargets[0].position);
    }

    void FindPlayer(NavMeshAgent agent, FieldOfView fov)
    {
        if(fov.visibleTargets.Count == 1)
        {
            ChaseTarget(agent,fov);
        //    agent.isStopped = true;
        }
        else
        {
        //    agent.isStopped = false;
            Patrol(agent);
        }
    }
    
    void Patrol(NavMeshAgent agent)
    {
        Vector3 position = moveSpots[indexSpot].transform.position;
        agent.SetDestination(position);
        if (Vector3.Distance(transform.position, position) < 0.5f)
        {
            if (waitTimePatrol <= 0)
            {
                indexSpot++;
                if (indexSpot > moveSpots.Count - 1)
                {
                    indexSpot = 0;
                }
                waitTimePatrol = startWaitTimePatrol;
            }
            else
            {
                waitTimePatrol -= Time.deltaTime;
            }
           
        }
    }
    

}
