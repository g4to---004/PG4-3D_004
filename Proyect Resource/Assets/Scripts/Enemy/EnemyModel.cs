﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModel
{
    private float _speed;
    public float speed {
        get {
            return _speed;
        }
        set {
            _speed = value;
        }
    }

    private bool _isActive;
    public bool isActive {
        get {
            return _isActive;
        }
        set {
            _isActive = value;
        }
    }

    private bool _playerFound;
    public bool playerFound {
        get {
            return _playerFound;
        }
        set {
            _playerFound = value;
        }
    }
    
    private float _stunCooldown;
    public float stunCooldown {
        get {
            return _stunCooldown;
        }
        set {
            _stunCooldown = value;
        }
    }

    public EnemyModel(float speed,float stunCooldown)
    {
        _speed = speed;
        _isActive = true;
        _playerFound = false;
        _stunCooldown = stunCooldown;
    }
    

    
    
   /* private void OnTriggerStay(Collider other) // TUNEAR //DAÑO ENEMIGO
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Player>().life--;
        }
    } */
}
