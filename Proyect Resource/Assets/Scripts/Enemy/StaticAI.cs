﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StaticAI : MonoBehaviour
{
    // PATRULLAR
    Vector3 origin;

    // Start is called before the first frame update
    void Start()
    {
        origin = transform.position;
    }
    
    public void StaticBehavior(NavMeshAgent agent, FieldOfView fov)
    {
        if (fov.visibleTargets.Count == 1)
        {
            ChaseTarget(agent,fov);
        }
        else
        {
            if (Vector3.Distance(transform.position, origin) < 1f)
            {
                agent.isStopped = true;
            }
            else
            {
                agent.isStopped = false;
                ReturnToOrigin(agent);
            }
        }
    }

    void ChaseTarget(NavMeshAgent agent, FieldOfView fov)
    {
        agent.isStopped = false;
        agent.SetDestination(fov.visibleTargets[0].position);
    }

    void ReturnToOrigin(NavMeshAgent agent)
    {
        agent.SetDestination(origin);
    }

}
