﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    EnemyModel model;
    EnemyView view;

    // START STATS
    public float speed;
    public float stunCooldown;
    NavMeshAgent agent;
    FieldOfView fov;
    float counterStun;

    // PATROL
    float waitTimePatrol;
    [HideInInspector]
    public float startWaitTimePatrol;
    public List<Transform> moveSpots;
    int indexSpot;

    // STATIC
    Vector3 origin;

    // IF PLAYER FOUND
    float stopLooking;
    public Transform targetPlayer;
    public float secondsToStopLooking;
    public bool someoneFoundPlayer;

    // TYPE ENEMY

    public enum TypeEnemy
    {
        Static,
        Patroller
    }

    public TypeEnemy typeEnemy;

    /*public EnemyController(EnemyModel model, EnemyView view)
    {
        this.model = model;
        this.view = view;
    }*/

    // Start is called before the first frame update
    void Start()
    {
        model = new EnemyModel(speed, stunCooldown);
        view = GetComponent<EnemyView>();
        agent = GetComponent<NavMeshAgent>();
        fov = GetComponent<FieldOfView>();
        counterStun = model.stunCooldown;
        origin = transform.position;
        stopLooking = secondsToStopLooking;
    }

    // Update is called once per frame
    void Update()
    {
        if (model.isActive)
        {
            Behavior();
        }
        else
        {
            counterStun -= Time.deltaTime;
            if (counterStun < 0)
            {
                model.isActive = true;
                agent.isStopped = false;
                counterStun = model.stunCooldown;
            }
        }

    }

    public void StunRevelation()
    {
        
        model.isActive = false;
        agent.isStopped = true;
    }


    void Behavior()
    {
        if (typeEnemy == TypeEnemy.Patroller)
        {
            PatrolBehavior();
        }

        if (typeEnemy == TypeEnemy.Static)
        {

            StaticBehavior();
        }
    }

    // PATROL
    void PatrolBehavior()
    {
        if(!someoneFoundPlayer)
            FindPlayer();
        // if player found
        if(someoneFoundPlayer)
    //    if (agent.isStopped)
        {
            ChaseTarget();
            FindPlayer(); // REVISA SI PUEDO VER PLAYER
        }
        else
        {
            Patrol();
        }
    }

    

    void ChaseTarget()
    {
        if(Vector3.Distance(transform.position,targetPlayer.position) < 3)
        {
            model.isActive = false;
            agent.isStopped = true;
        }
        else
        {
            model.isActive = true;
            agent.isStopped = false;
            agent.SetDestination(targetPlayer.position);
        }

        
    }

    void FindPlayer()
    {
        if (fov.visibleTargets.Count == 1)
        {
            targetPlayer = fov.visibleTargets[0];
            GameObject[] allEnemy = GameObject.FindGameObjectsWithTag("Enemy");
           
            foreach (GameObject singleEnemy in allEnemy)
            {
                EnemyController controllerEnemy = singleEnemy.GetComponent<EnemyController>();
                controllerEnemy.someoneFoundPlayer = true;
                controllerEnemy.targetPlayer = targetPlayer;
            }
            // ALERT ALL OTHER ENEMIES
        //    ChaseTarget();
        }
        else
        {
            // WAIT TO CHASE
            stopLooking -= Time.deltaTime;
            if (stopLooking < 0)
            {
                GameObject[] allEnemy = GameObject.FindGameObjectsWithTag("Enemy");

                foreach (GameObject singleEnemy in allEnemy)
                {
                    EnemyController controllerEnemy = singleEnemy.GetComponent<EnemyController>();
                    controllerEnemy.someoneFoundPlayer = false;
                    controllerEnemy.targetPlayer = null;
                }
                stopLooking = secondsToStopLooking;
            }


        }
    }

    void Patrol()
    {
        Vector3 position = moveSpots[indexSpot].transform.position;
        agent.SetDestination(position);
        if (Vector3.Distance(transform.position, position) < 1f)
        {
            if (waitTimePatrol <= 0)
            {
                indexSpot++;
                if (indexSpot > moveSpots.Count - 1)
                {
                    indexSpot = 0;
                }
                waitTimePatrol = startWaitTimePatrol;
            }
            else
            {
                waitTimePatrol -= Time.deltaTime;
            }

        }
    }

    // STATIC
    void StaticBehavior()
    {
        if (!someoneFoundPlayer)
            FindPlayer();

        if (model.playerFound || someoneFoundPlayer)
        {
            agent.isStopped = false;
            ChaseTarget();
        }
        else
        {
            if (Vector3.Distance(transform.position, origin) < 1f)
            {
                agent.isStopped = true;
            }
            else
            {
                agent.isStopped = false;
                ReturnToOrigin();
            }
        }
    }

    void ReturnToOrigin()
    {
        agent.SetDestination(origin);
    }
}
