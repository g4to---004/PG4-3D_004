﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondObject : MonoBehaviour
{
    public int num;
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Objeto1")
        {
            if(other.gameObject.GetComponent<FirstObject>().num == num)
            {
                Destroy(gameObject);
            }
        }
    }
}
