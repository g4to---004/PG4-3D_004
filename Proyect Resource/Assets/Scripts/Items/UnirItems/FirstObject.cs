﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstObject : MonoBehaviour
{
    public GameObject objCreado;
    public GameObject objCreadoSpawnpoint;
    public int num;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Objeto2")
        {
            if(other.gameObject.GetComponent<SecondObject>().num == num)
            {
                GameObject temporalBullet = Instantiate(objCreado);
                temporalBullet.transform.position = objCreadoSpawnpoint.transform.position;
                Destroy(gameObject);
            }
        }
    }
}
