﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TerminarNivel : MonoBehaviour
{
    private bool _fin;
    private bool _fadeExit;
    public GameObject _fadeF;
    public GameObject Camera;

    private float _timeToNewScene;

    private void Start()
    {
        _fin = false;
        _timeToNewScene = 2.3f;
    }
    private void Update()
    {
        if (_fin)
        {
            if (_fadeExit)
            {
                Vector3 FadePosition = new Vector3(Camera.transform.position.x, Camera.transform.position.y, Camera.transform.position.z + 1);

                GameObject objetoHijo = Instantiate(_fadeF, FadePosition, Camera.transform.rotation) as GameObject;
                objetoHijo.transform.parent = Camera.transform;
                objetoHijo.transform.position = FadePosition;

                _fadeExit = false;
            }
            if (_timeToNewScene <= 0)
            {
                SceneManager.LoadScene("Menu");
            }
            else
            {
                Time.timeScale = 1f;
                _timeToNewScene -= Time.deltaTime;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Fin del juego")
        {
            _fin = true;
        }
    }
}
