﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Pausa : MonoBehaviour {

    private bool _pausa;

    public GameObject _pausaBanner;
    public GameObject _fadeSalida;
    public GameObject Camera;

    public bool _exit;
    public bool _isPause;

    private bool _fadeExit;

    private float _timeToNewScene;
    public bool _nuevaAccion;

	void Start ()
    {
        _isPause = false;
        _exit = false;
        _pausa = false;
        _timeToNewScene = 2.3f;
        _nuevaAccion = false;
    }
	
	void Update ()
    {
        MenuPausa();
        ExitApp();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _pausa = !_pausa;
        }
    }
    void MenuPausa()
    {
        if (_pausa && !_nuevaAccion)
        {
            _isPause = true;
            _pausaBanner.SetActive(true);
            Time.timeScale = 0f;
        }
        else if (!_nuevaAccion)
        {
            _isPause = false;
            _pausaBanner.SetActive(false);
            Time.timeScale = 1f;
        }
    }
    public void ResumeButton()
    {
        _pausa = false;
    }
    public void ExitPressButton()
    {
        _exit = true;
        _fadeExit = true;
    }
    void ExitApp()
    {
        if (_exit)
        {
            _nuevaAccion = true;
            if (_fadeExit)
            {
                Vector3 FadePosition = new Vector3(Camera.transform.position.x, Camera.transform.position.y, Camera.transform.position.z + 1);

                GameObject objetoHijo = Instantiate(_fadeSalida, FadePosition, Camera.transform.rotation) as GameObject;
                objetoHijo.transform.parent = Camera.transform;
                objetoHijo.transform.position = FadePosition;

                _fadeExit = false;
            }
            if (_timeToNewScene <= 0)
            {
                SceneManager.LoadScene("Menu");
            }
            else
            {
                _pausa = false;
                Time.timeScale = 1f;
                _timeToNewScene -= Time.deltaTime;
            }
        }
    }
}
