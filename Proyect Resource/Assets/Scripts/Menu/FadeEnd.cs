﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeEnd : MonoBehaviour {

    public Material ColorInicial;
    [Range(0,1)]
    public float Speed = 0.5f;

    private float alpha = 0f;

    // Use this for initialization
    void Start()
    {
        alpha = 0f;
        gameObject.GetComponent<Renderer>().material = ColorInicial;
        ColorInicial.color = new Color(0, 0, 0, alpha);
    }

    // Update is called once per frame
    public void Update()
    {
        gameObject.GetComponent<Renderer>().material = ColorInicial;
        ColorInicial.color = new Color(0, 0, 0, alpha);
        alpha = (alpha < 1) ? alpha += Speed * Time.deltaTime : alpha = 1;
    }
}
