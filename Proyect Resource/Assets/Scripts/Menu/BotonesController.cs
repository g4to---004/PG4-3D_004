﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BotonesController : MonoBehaviour {

    public GameObject _fadeExitScene;

    public string _nivelName;

    private bool _botonActivadoPlay;
    private bool _botonActivadoExit;

    private bool _activarBoton;
    private bool _cambio;

    [Range(0,2)]
    public float _initTimerFade;
    private double _timerFade;

	void Start ()
    {
        _timerFade = _initTimerFade;
        _botonActivadoPlay = _botonActivadoExit = false;
        _activarBoton = false;
        _cambio = false;
        Cursor.visible = true;
    }
	
	void Update ()
    {
        ActivarBoton();
    }
    void ActivarBoton()
    {
        if (_botonActivadoPlay)
        {
            _timerFade -= Time.deltaTime * 0.5;
            if (_activarBoton)
            {
                Vector3 FadePosition = new Vector3(0, 0, -3);
                Instantiate(_fadeExitScene, FadePosition, transform.localRotation);
                _activarBoton = false;
            }
            if(_timerFade <= 0)
            {
                SceneManager.LoadScene(_nivelName);
            }
        }
        if (_botonActivadoExit)
        {
            _timerFade -= Time.deltaTime * 0.5;
            if (_activarBoton)
            {
                Vector3 FadePosition = new Vector3(0, 0, -3);
                Instantiate(_fadeExitScene, FadePosition, transform.localRotation);
                _activarBoton = false;
            }
            if (_timerFade <= 0)
            {
                Debug.Log("cerrar juego");
                Application.Quit();
            }
        }
    }
    public void BotonPlay()
    {
        if (!_cambio)
        {
            _botonActivadoPlay = true;
            _activarBoton = true;
            _cambio = true;
        }
    }
    public void BotonExit()
    {
        if (!_cambio)
        {
            _botonActivadoExit = true;
            _activarBoton = true;
            _cambio = true;
        }
    }
}
