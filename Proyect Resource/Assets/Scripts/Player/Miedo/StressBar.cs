﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StressBar : MonoBehaviour
{
    public GameObject _bar;
    public ActivarMiedo _miedo;
    public Pausa _gameOver;
    public float _num;

    private void Start()
    {
        _num = 1;
    }
    void Update()
    {
        _bar.transform.localScale = new Vector3(_num, _bar.transform.localScale.y, 1);

        if (_miedo._activarMiedo)
        {
            if(_num > 0)
            {
                _num -= Time.deltaTime / 10;
            }
            else
            {
                _gameOver._exit = true;
            }
        }
        else
        {
            if(_num < 1)
            {
                _num += Time.deltaTime / 12;
            }
            else
            {
                _num = 1;
            }
        }
    }
}
