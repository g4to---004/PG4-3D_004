﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using Cinemachine;

public class ActivarMiedo : MonoBehaviour
{
    public PostProcessingProfile _gameplayPsPr;
    public CinemachineVirtualCamera _camara;

    [Range(0, 1)]
    public float _intensity = 0;
    public bool _activarMiedo;

    void Start()
    {
        _intensity = 0;
        _activarMiedo = false;
    }

    void Update()
    {
        IntensidadChromatica(_intensity);
        ActivarRecuerdo();
    }
    void ActivarRecuerdo()
    {
        if (_activarMiedo)
        {
            _intensity = (_intensity < 1) ? _intensity += Time.deltaTime : _intensity = 1;
            _camara.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0.1f;
            _camara.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1;
        }
        else
        {
            _intensity = (_intensity > 0) ? _intensity -= Time.deltaTime : _intensity = 0;
            _camara.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
            _camara.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
        }
    }
    public void IntensidadChromatica(float _intensidad)
    {
        if (_gameplayPsPr.chromaticAberration.enabled)
        {
            ChromaticAberrationModel.Settings newSettings = _gameplayPsPr.chromaticAberration.settings;
            newSettings.intensity = _intensidad;
            _gameplayPsPr.chromaticAberration.settings = newSettings;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            _activarMiedo = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            _activarMiedo = false;
        }
    }
}
