﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightModel
{
    private float _lightIntensity;
    public float lightIntensity {
        get {
            return _lightIntensity;
        }
        set {
            _lightIntensity = value;
        }
    }

    private bool _focusOn;
    public bool focusOn {
        get {
            return _focusOn;
        }
        set {
            _focusOn = value;
        }
    }

    private bool _rainUpgrade;
    public bool rainUpgrade {
        get {
            return _rainUpgrade;
        }
        set {
            _rainUpgrade = value;
        }
    }

    public FlashLightModel(float light)
    {
        _lightIntensity = light;
        _focusOn = false;
        _rainUpgrade = true;
    }
}
