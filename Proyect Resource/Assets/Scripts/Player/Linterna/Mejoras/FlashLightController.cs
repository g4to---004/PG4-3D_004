﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightController : MonoBehaviour
{
    public FlashLightModel model;

    public Light myLight;

    //customizas los limites de intensidad
    [Range(0,10)]
    public float startIntensity;
    [Range(0,5)]
    public float rainDegradeIntensity;
    

    [Range(0,50)]
    public float focusAngle;
    [Range(0,100)]
    public float unfocusAngle;

    //custom range
    [Range(0, 50)]
    public float normalRange;
    [Range(0, 100)]
    public float focusRange;

    //Controla la luz
    private float currentLight;

    public void Start()
    {
        model = new FlashLightModel(startIntensity);

        currentLight = startIntensity;
    }
    private void Update()
    {
        myLight.intensity = currentLight;
        ToggleRainUpgrade();
        LightIntensity();
        EnfoqueLamp();
    }
    void ToggleRainUpgrade()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (model.rainUpgrade)
            {
                model.rainUpgrade = false;
            }
            else
            {
                model.rainUpgrade = true;
            }
        }
    }

    void LightIntensity()
    {
        if (model.rainUpgrade)
        {
            if (currentLight < startIntensity)
            {
                currentLight += Time.deltaTime;
            }
            else
            {
                currentLight = startIntensity;
            }
        }
        else
        {
            if (currentLight > rainDegradeIntensity)
            {
                currentLight -= Time.deltaTime * 2;
            }
            else
            {
                currentLight = rainDegradeIntensity;
            }
        }
    }
    void EnfoqueLamp()
    {
        if (model.focusOn)
        {
            myLight.spotAngle = focusAngle;
            myLight.intensity = myLight.intensity + 5;
            myLight.range = focusRange;

        }
        else
        {
            myLight.spotAngle = unfocusAngle;
            myLight.range = normalRange;
        }
    }
}
