﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    public GameObject _item;
    Transform _player;
    Vector3 spawnPos;
    Vector3 playerDir;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        
    }

    public void SpawnDroppedItem()
    {
        playerDir = _player.forward;
        spawnPos = _player.position + (playerDir)*1.5f;
   //     Vector3 playerPos = new Vector3(_camara.transform.position.x, 1, _camara.transform.position.z + 1);
        Instantiate(_item, spawnPos, Quaternion.identity);
    }
}
