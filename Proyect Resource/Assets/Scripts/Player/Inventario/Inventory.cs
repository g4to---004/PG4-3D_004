﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour, IItemContainer
{
    public bool[] _isFull;
    public GameObject[] _slots;

    public bool AddItem(Item _item)
    {
        for (int i = 0; i < _slots.Length - 2; i++)
        {
            if(_slots[i] == null)
            {
                _slots[i] = _slots[i];
                return false;
            }
        }
        return true;
    }

    public bool CanAddItem(Item item, int amount = 1)
    {
        throw new System.NotImplementedException();
    }

    public void Clear()
    {
        throw new System.NotImplementedException();
    }

    public bool ContainsItem(Item _item)
    {
        for (int i = 0; i < _slots.Length - 2; i++)
        {
            if (_slots[i] == null)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsFull()
    {
        for (int i = 0; i < _slots.Length - 2; i++)
        {
            if (_slots[i] == null)
            {
                return false;
            }
        }
        return true;
    }

    public int ItemCount(Item _item)
    {
        int number = 0;

        for (int i = 0; i < _slots.Length - 2; i++)
        {
            if (_slots[i] == null)
            {
                number++;
            }
        }
        return number;
    }

    public int ItemCount(string itemID)
    {
        throw new System.NotImplementedException();
    }

    public bool RemoveItem(Item _item)
    {
        for (int i = 0; i < _slots.Length - 2; i++)
        {
            if (_slots[i] == null)
            {
                _slots[i] = null;
                return false;
            }
        }
        return true;
    }

    public Item RemoveItem(string itemID)
    {
        throw new System.NotImplementedException();
    }
}
