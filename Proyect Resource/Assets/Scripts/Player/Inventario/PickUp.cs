﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    private Inventory _inventory;
    public GameObject itemButton;

    private void Start()
    {
        _inventory = GameObject.FindGameObjectWithTag("UI").GetComponent<Inventory>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            for (int i = 0; i < _inventory._slots.Length; i++)
            {
                if (!_inventory._isFull[i])
                {
                    //item can be add to the inventory
                    _inventory._isFull[i] = true;
                    Instantiate(itemButton, _inventory._slots[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
}
