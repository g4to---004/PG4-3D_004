﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlice : MonoBehaviour
{
    public bool _inventario;
    public RectTransform _panel;
    public GameObject _player;
    public float _speed;

    public Vector2 _limits;

    private void Start()
    {
        _inventario = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            _inventario = !_inventario;
        }
        SliceInventory();
    }
    void SliceInventory()
    {
        if (_inventario)
        {
            Vector3 MoveSlide = _panel.localPosition;
            if(_panel.localPosition.x < _limits.y)
            {
                MoveSlide.x += Time.fixedDeltaTime * 500 * _speed;
            }
            _panel.localPosition = MoveSlide;
            Cursor.visible = false;
            if(_player != null)
            {
                _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
                _player.GetComponent<PlayerController>().enabled = true;
            }
        }
        else
        {
            Vector3 MoveSlide = _panel.localPosition;
            if(_panel.localPosition.x > _limits.x)
            {
                MoveSlide.x -= Time.fixedDeltaTime * 500 * _speed;
            }
            _panel.localPosition = MoveSlide;
            Cursor.visible = true;
            if (_player != null)
            {
                _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
                _player.GetComponent<PlayerController>().enabled = false;
            }
        }
    }
}
