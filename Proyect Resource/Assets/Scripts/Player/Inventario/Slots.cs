﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slots : MonoBehaviour, IDropHandler
{
    public GameObject _item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }
    private Inventory _inventory;
    public int _i;
    public bool _drop;

    private void Start()
    {
        _inventory = GameObject.FindGameObjectWithTag("UI").GetComponent<Inventory>();
    }

    private void Update()
    {
        if (transform.childCount <= 0)
        {
            _inventory._isFull[_i] = false;
        }
        else
        {
            _inventory._isFull[_i] = true;
        }
        if (_drop)
        {
            DropItem();
            _drop = false;
        }
    }
    public void DropItem()
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<DropItem>().SpawnDroppedItem();
            GameObject.Destroy(child.gameObject);
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!_item)
        {
            MoveItem._itemIcon.transform.SetParent(transform);
        }
    }
}
