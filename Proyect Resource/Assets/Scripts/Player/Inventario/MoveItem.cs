﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveItem : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject _itemIcon;
    Vector3 _startPos;

    Transform _startParent;

    public void OnBeginDrag(PointerEventData eventData)
    {
        _itemIcon = gameObject;
        _startPos = transform.position;
        _startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _itemIcon = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent == _startParent)
        {
            //transform.position = _startPos;
            transform.GetComponentInParent<Slots>().DropItem();
        }
    }
}
