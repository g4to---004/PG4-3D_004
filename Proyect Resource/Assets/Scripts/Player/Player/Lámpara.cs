﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lámpara : MonoBehaviour
{
    public GameObject Luz;
    public GameObject cap;

    public bool encendida = false;
    public bool puedeEncender = false;

    // UPGRADE
    public bool rainUpgrade = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (encendida == false && Input.GetKeyDown(KeyCode.F) && puedeEncender == true)
        {
            Luz.SetActive(true);
            encendida = true;
            cap.SetActive(false);

        }
        else
            if (encendida == true && Input.GetKeyDown(KeyCode.F) && puedeEncender == true)
            {
            Luz.SetActive(false);
            cap.SetActive(true);
            encendida = false;
            }

    }




    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            puedeEncender = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            puedeEncender = false;
        }
    }

}
