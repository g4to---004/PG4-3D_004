﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    PlayerModel model;
    PlayerView view;
    FlashLightController flashControl;
    public int startHealth;

    // MOVE
    public float speed;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;

    // CAMERA SPEED
    public float cameraHorizontalSpeed;
    public float cameraVerticalSpeed;
    float xAxisClamp;
    float h, v;

    // CROUCH
    public float crouchDelta;
    public Transform normalPositionCamera;
    public Transform crouchPositionCamera;
    FieldOfView fov;

    // MOVEMENT
    [HideInInspector]
    public bool canMove;



    // Start is called before the first frame update

    public PlayerController(PlayerModel player, PlayerView playerView)
    {
        this.model = player;
        this.view = playerView;
    }

    void Start()
    {
        model = new PlayerModel(startHealth,speed);
        view = GetComponent<PlayerView>();
        flashControl = view.flashLight.GetComponent<FlashLightController>();
        fov = GetComponent<FieldOfView>();
        xAxisClamp = 0.0f;
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        CheckDeath();
        InputMove();
        InputBodyRotate();
        InputCameraLook();
        InputFlashlight();
        InputCrouch();
        InputFocusFlashlight();
        InputOpenDoor();
    }
    
    void InputOpenDoor()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            GameObject doorFound = DetectDoor();
            if (doorFound != null && doorFound.tag == "Door")
            {
                doorFound.GetComponentInParent<DoorController>().OpenDoor();
            }
        }
    }

    GameObject DetectDoor()
    {
        GameObject target = null;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 4f) == true)
        {
            target = hit.collider.gameObject;
        }
        return target;
    }

    void InputFocusFlashlight()
    {
        if (Input.GetKey(KeyCode.G)) // MOVER A PLAYER CONTROLLER
        {
            flashControl.model.focusOn = true;
            
             foreach (Transform enemy in fov.visibleTargets)
                {
                Debug.Log("See");
                EnemyController controllerEnemy = enemy.GetComponent<EnemyController>();
                controllerEnemy.StunRevelation();
                }
                // ALERT ALL OTHER ENEMIES
                //    ChaseTarget();
            

            // FIND ALL VIEW
            //   StunAllView();
            // STUN ALL ENEMIES ON VIEW PLAYER...!!
        }
        else
        {
            flashControl.model.focusOn = false;
        }
    }

    void CheckDeath()
    {
        if (model.health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void InputCrouch()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            model.crouching = true;
            view.StartCrouch(crouchDelta);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            model.crouching = false;
            view.StopCrouch(crouchDelta);
        }

        view.Crouching(model.crouching, crouchPositionCamera.localPosition, normalPositionCamera.localPosition, crouchDelta);
    }

    void InputMove()
    {
        if(Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            view.PlayerMove(model.speed, h, v, gravity, maxVelocityChange);
        }
    }

    void InputBodyRotate()
    {
        if(Input.GetAxis("Mouse X") != 0f)
        {
            h = cameraHorizontalSpeed * Input.GetAxis("Mouse X");
            view.RotateBody(h);
        }
    }

    void InputCameraLook()
    {
        if(Input.GetAxis("Mouse Y") != 0f)
        {
            v = cameraVerticalSpeed * Input.GetAxis("Mouse Y");
            view.RotateCamera(v);
            PerformingCameraClamp();
        }
    }

    void PerformingCameraClamp()
    {
        xAxisClamp += v;
        if (xAxisClamp > 90.0f)
        {
            xAxisClamp = 90.0f;
            v = 0.0f;
            view.ClampXAxisRotation(270.0f);
        }
        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            v = 0.0f;
            view.ClampXAxisRotation(90.0f);
        }
    }

    void InputFlashlight()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (model.canUseFlashlight)
            {
                view.ToggleFlashlight();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Debug.Log("Ow");
          //  model.health--;
        }
    }

}
