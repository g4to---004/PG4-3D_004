﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    Rigidbody playerRigidbody;
    CapsuleCollider boxCollider;
 //   BoxCollider boxCollider;
    public GameObject flashLight;
    public GameObject playerCamera;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        playerRigidbody = GetComponent<Rigidbody>();
        boxCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayerMove(float speed, float h, float v, float gravity, float maxVelocityChange)
    {
        Vector3 targetVelocity = new Vector3(h, 0, v);
        targetVelocity = transform.TransformDirection(targetVelocity);
        targetVelocity *= speed; // sacar del model player

        // Apply a force that attempts to reach our target velocity
        Vector3 velocity = playerRigidbody.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
        velocityChange.y = 0;
        playerRigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

        // We apply gravity manually for more tuning control
        playerRigidbody.AddForce(new Vector3(0, -gravity * playerRigidbody.mass, 0));
    }

    public void StartCrouch(float crouchDelta)
    {
        boxCollider.height -= crouchDelta;
    //    boxCollider.size -= new Vector3(0, crouchDelta, 0);
        boxCollider.center -= new Vector3(0, crouchDelta / 2, 0);
    }

    public void StopCrouch(float crouchDelta)
    {
        boxCollider.height += crouchDelta;
        //   boxCollider.size += new Vector3(0, crouchDelta, 0);
        boxCollider.center += new Vector3(0, crouchDelta / 2, 0);
    }


    public void RotateBody(float horizontal)
    {
        transform.Rotate(0, horizontal, 0);
    }

    public void RotateCamera(float vertical)
    {
        playerCamera.transform.Rotate(-vertical, 0, 0);
    }

    public void ClampXAxisRotation(float value)
    {
        Vector3 eulerRotation = playerCamera.transform.eulerAngles;
        eulerRotation.x = value;
        playerCamera.transform.eulerAngles = eulerRotation;
    }


    public void Crouching(bool crouching, Vector3 crouch, Vector3 normal, float crouchDelta)
    {
        if (crouching)
        {
            if (playerCamera.transform.localPosition.y > crouch.y)
            {
                if (playerCamera.transform.localPosition.y - (crouchDelta * Time.deltaTime * 8) < crouch.y)
                {
                    playerCamera.transform.localPosition = flashLight.transform.localPosition = crouch;
                }
                else
                {
                    float newY = crouchDelta * Time.deltaTime * 8;
                    Vector3 dir = new Vector3(0, newY, 0);
                    playerCamera.transform.localPosition -= dir;
                    flashLight.transform.localPosition -= dir;
                }
            }
        }
        else
        {
            
            if (playerCamera.transform.localPosition.y < normal.y)
            {
                if (playerCamera.transform.localPosition.y + (crouchDelta * Time.deltaTime * 8) > normal.y)
                {
                    playerCamera.transform.localPosition = flashLight.transform.localPosition = normal;
                }
                else
                {
                    float newY = crouchDelta * Time.deltaTime * 8;
                    Vector3 dir = new Vector3(0, newY, 0);
                    playerCamera.transform.localPosition += dir;
                    flashLight.transform.localPosition += dir;
                }
            }

        }
    }

    public void ToggleFlashlight()
    {
        if(flashLight.activeSelf == true)
        {
            flashLight.SetActive(false);
        }
        else
        {
            flashLight.SetActive(true);
        }
    }
}
