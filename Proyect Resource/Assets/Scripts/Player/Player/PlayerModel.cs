﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel
{
    private int _health;
    public int health {
        get {
            return _health;
        }
        set {
            _health = value;
        }
    }
    private float _speed;
    public float speed {
        get {
            return _speed;
        }
        set {
            _speed = value;
        }
    }

    private bool _crouching;
    public bool crouching {
        get {
            return _crouching;
        }
        set {
            _crouching = value;
        }
    }

    private bool _canUseFlashlight;
    public bool canUseFlashlight {
        get {
            return _canUseFlashlight;
        }
        set {
            _canUseFlashlight = value;
        }
    }
    


    public PlayerModel(int health,float speed)
    {
        _health = health;
        _speed = speed;
        _crouching = false;
        _canUseFlashlight = true;
    }
    
}
