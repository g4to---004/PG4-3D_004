﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecolectarRecurso : MonoBehaviour
{
    public bool equipedBackpack;

    private int _selectorItem;
    public Text[] _indicacionDeObjetos;

    public string[] _objetosRecolectables;

    public int[] _cantidadItems = new int[3];
    private bool _cogerObjeto;

    private void Start()
    {
        equipedBackpack = false;
        for (int i = 0; i < _indicacionDeObjetos.Length; i++)
        {
            if(_indicacionDeObjetos[i] != null)
            {
                _indicacionDeObjetos[i].text = _objetosRecolectables[i] + ": " + 0;
            }
        }
    }
    void Update()
    {
        if (_cogerObjeto)
            {
                ActualizarItems();
                _cogerObjeto = false;
            }
    }
    void ActualizarItems()
    {
        _cantidadItems[_selectorItem]++;
        _indicacionDeObjetos[_selectorItem].text = _objetosRecolectables[_selectorItem] + ": " + _cantidadItems[_selectorItem];
    }

    // COGE ITEM
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Recurso")
        {
            if (equipedBackpack)
            {
                _selectorItem = collision.gameObject.GetComponent<DetectorItem>()._numItem;
                _cogerObjeto = true;
                Destroy(collision.gameObject);
            }

        }

        if (collision.gameObject.tag == "Backpack")
        {
            equipedBackpack = true;
            Destroy(collision.gameObject);
        }
    }
    
    
}
