﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct ItemAmount
{
    public Item Item;
    [Range(1, 999)]
    public int amount;
}

[CreateAssetMenu]
public class CraftingRecipe : ScriptableObject
{
    public List<ItemAmount> _material;
    public List<ItemAmount> _results;

    public bool CanCraft(IItemContainer _itemContainer)
    {
        return HasMaterials(_itemContainer) && HasSpace(_itemContainer);
    }
    private bool HasMaterials(IItemContainer itemContainer)
    {
        foreach (ItemAmount itemAmount in _material)
        {
            if (itemContainer.ItemCount(itemAmount.Item.ID) < itemAmount.amount)
            {
                Debug.LogWarning("You don't have the required materials.");
                return false;
            }
        }
        return true;
    }

    private bool HasSpace(IItemContainer itemContainer)
    {
        foreach (ItemAmount itemAmount in _results)
        {
            if (!itemContainer.CanAddItem(itemAmount.Item, itemAmount.amount))
            {
                Debug.LogWarning("Your inventory is full.");
                return false;
            }
        }
        return true;
    }
    public void Craft(IItemContainer _itemContainer)
    {
        if(CanCraft(_itemContainer))
        {
            foreach (ItemAmount ItemAmount in _material)
            {
                for (int i = 0; i < ItemAmount.amount; i++)
                {
                    _itemContainer.RemoveItem(ItemAmount.Item);
                }
            }

            foreach (ItemAmount ItemAmount in _results)
            {
                for (int i = 0; i < ItemAmount.amount; i++)
                {
                    _itemContainer.AddItem(ItemAmount.Item);
                }
            }
        }
    }
    private void RemoveMaterials(IItemContainer itemContainer)
    {
        foreach (ItemAmount itemAmount in _material)
        {
            for (int i = 0; i < itemAmount.amount; i++)
            {
                Item oldItem = itemContainer.RemoveItem(itemAmount.Item.ID);
                oldItem.Destroy();
            }
        }
    }
    private void AddResults(IItemContainer itemContainer)
    {
        foreach (ItemAmount itemAmount in _results)
        {
            for (int i = 0; i < itemAmount.amount; i++)
            {
                itemContainer.AddItem(itemAmount.Item.GetCopy());
            }
        }
    }
}
