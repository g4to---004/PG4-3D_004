﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformarBasura : MonoBehaviour
{
    public GameObject initialObject;
    public GameObject swapObject;

    Mesh initialMesh;
    Mesh swapMesh;
    GameObject theTarget;

    // Start is called before the first frame update
    void Start()
    {
        theTarget = initialObject;

        initialMesh = initialObject.GetComponent<MeshFilter>().mesh;
        swapMesh = swapObject.GetComponent<MeshFilter>().mesh;

        theTarget.GetComponent<MeshFilter>().mesh = swapMesh;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Si colisiona con el player el enemigo.....
            
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Si colisiona con el player el enemigo.....
        }
    }
}
