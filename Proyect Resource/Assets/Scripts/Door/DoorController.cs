﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float speed;
    public float angle;
    private float _normalAngle;
    private float _newAngle;
    public Vector3 direction;
    public bool isOpen;

    void Start()
    {
        _normalAngle = transform.localEulerAngles.y;
        _newAngle = transform.localEulerAngles.y + 90;
    }

    void Update()
    {
        if (Mathf.Round(transform.localEulerAngles.y) != angle)
        {
            transform.Rotate(direction * speed);
        }
    }

    public void OpenDoor()
    {
        if (isOpen)
        {
            angle = _normalAngle;
            direction = Vector3.down;
            isOpen = false;
        }
        else
        {
            angle = _newAngle;
            direction = Vector3.up;
            isOpen = true;
        }
    }

}
