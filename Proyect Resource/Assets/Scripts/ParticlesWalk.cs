﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesWalk : MonoBehaviour
{
    public GameObject ParWalk1;
    public GameObject ParWalk2;
    public GameObject ParWalk3;
    public GameObject ParWalk4;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        ActivarParticulas();
        DesactivarParticulas();
    }
    void ActivarParticulas()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            ParWalk1.SetActive(true);
        }   
        if (Input.GetKeyDown(KeyCode.S))
        {
            ParWalk2.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            ParWalk3.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            ParWalk4.SetActive(true);
        }
    }
    void DesactivarParticulas()
    {
        if (Input.GetKeyUp(KeyCode.W))
        {
            ParWalk1.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            ParWalk2.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            ParWalk3.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            ParWalk4.SetActive(false);
        }
    }

}
