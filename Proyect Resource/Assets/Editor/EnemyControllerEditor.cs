﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(EnemyController)),CanEditMultipleObjects]
public class EnemyControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        EnemyController controller = target as EnemyController;
        if(controller.typeEnemy == EnemyController.TypeEnemy.Patroller)
        {
            controller.startWaitTimePatrol = EditorGUILayout.FloatField("Wait Time",controller.startWaitTimePatrol);
        }
    }
}
