﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    private void OnSceneGUI()
    {
        FieldOfView agent = (FieldOfView)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(agent.transform.position, Vector3.up, Vector3.forward, 360, agent.radius);
        Vector3 viewAngleA = agent.DirFromAngle(-agent.angleView / 2, false);
        Vector3 viewAngleB = agent.DirFromAngle(agent.angleView / 2, false);
        Handles.DrawLine(agent.transform.position, agent.transform.position + viewAngleA * agent.radius);
        Handles.DrawLine(agent.transform.position, agent.transform.position + viewAngleB * agent.radius);
        Handles.color = Color.red;

        foreach(Transform visibleTarget in agent.visibleTargets)
        {
            Handles.DrawLine(agent.transform.position, visibleTarget.transform.position);
        }
    }
}
